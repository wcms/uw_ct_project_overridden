<?php

/**
 * @file
 * uw_ct_project_overridden.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_project_overridden_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_instance.
  $overrides["field_instance.node-uw_project-field_project_direction.label"] = 'Project IT Direction';

  // Exported overrides for: menu_links.
  $overrides["menu_links.menu-site-manager-vocabularies_strategic-alignment:admin/structure/taxonomy/strategic_alignment.customized"] = 1;
  $overrides["menu_links.menu-site-manager-vocabularies_strategic-alignment:admin/structure/taxonomy/strategic_alignment.link_title"] = 'Project IT Direction';

  // Exported overrides for: taxonomy.
  $overrides["taxonomy.strategic_alignment.description"] = '';
  $overrides["taxonomy.strategic_alignment.name"] = 'Project IT Direction';

  // Exported overrides for: views_view.
  $overrides["views_view.strategic_alignment_block.display|default|display_options|title"] = 'Projects by IT direction';

  return $overrides;
}
