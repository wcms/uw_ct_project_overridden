<?php

/**
 * @file
 * uw_ct_project_overridden.features.inc
 */

/**
 * Implements hook_field_default_field_instances_alter().
 */
function uw_ct_project_overridden_field_default_field_instances_alter(&$data) {
  if (isset($data['node-uw_project-field_project_direction'])) {
    $data['node-uw_project-field_project_direction']['label'] = 'Project IT Direction'; /* WAS: 'Strategic Alignment' */
  }
}

/**
 * Implements hook_menu_default_menu_links_alter().
 */
function uw_ct_project_overridden_menu_default_menu_links_alter(&$data) {
  if (isset($data['menu-site-manager-vocabularies_strategic-alignment:admin/structure/taxonomy/strategic_alignment'])) {
    $data['menu-site-manager-vocabularies_strategic-alignment:admin/structure/taxonomy/strategic_alignment']['customized'] = 1; /* WAS: 0 */
    $data['menu-site-manager-vocabularies_strategic-alignment:admin/structure/taxonomy/strategic_alignment']['link_title'] = 'Project IT Direction'; /* WAS: 'Strategic alignment' */
  }
}

/**
 * Implements hook_taxonomy_default_vocabularies_alter().
 */
function uw_ct_project_overridden_taxonomy_default_vocabularies_alter(&$data) {
  if (isset($data['strategic_alignment'])) {
    $data['strategic_alignment']['description'] = ''; /* WAS: '' */
    $data['strategic_alignment']['name'] = 'Project IT Direction'; /* WAS: 'Strategic Alignment' */
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function uw_ct_project_overridden_views_default_views_alter(&$data) {
  if (isset($data['strategic_alignment_block'])) {
    $data['strategic_alignment_block']->display['default']->display_options['title'] = 'Projects by IT direction'; /* WAS: 'Strategic alignment' */
  }
}
